package explorer

import "gitlab.com/nitronick600/siacorn/modules"

// Alerts implements the modules.Alerter interface for the explorer.
func (e *Explorer) Alerts() []modules.Alert {
	return []modules.Alert{}
}
