package miner

import "gitlab.com/nitronick600/siacorn/modules"

// Alerts implements the modules.Alerter interface for the miner.
func (m *Miner) Alerts() []modules.Alert {
	return []modules.Alert{}
}
