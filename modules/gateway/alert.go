package gateway

import "gitlab.com/nitronick600/siacorn/modules"

// Alerts implements the modules.Alerter interface for the gateway.
func (g *Gateway) Alerts() []modules.Alert {
	return g.staticAlerter.Alerts()
}
